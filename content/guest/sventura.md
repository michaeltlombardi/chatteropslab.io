+++
title = "Samantha Ventura"
Website = "https://devopslibrary.com"
Twitter = ""
YouTube = ""
Thumbnail = ""
Instagram = ""
Pinterest = ""
Type = "guest"
Facebook = ""
GitHub = ""
Linkedin = ""
date = "2017-07-06T16:15:20-05:00"

+++

Originally from San Diego, CA, English Professor, in the field of higher education for 15 years. Specialized in the areas of adult learning and organizational leadership, with particular expertise in bridging the gaps between leader and follower. BA in English, MA in Teaching (MA.T), and PhD in Organizational Leadership. Co-Founder of the DevOps Library. Lover of travel, books, meaningful conversation, dachshunds, her two children, gratitude, and random acts of kindness.