+++
title = "Dustin Collins"
Website = "http://dustinrcollins.com/"
Twitter = "dustinmm80"
YouTube = ""
Thumbnail = "https://media.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAAUOAAAAJDkxMDQzMjMxLWUwNmEtNGVjYi05ZDkyLWJhNGJjOTQwMjU2OQ.jpg"
Instagram = ""
Pinterest = ""
Type = "guest"
Facebook = ""
GitHub = "dustinmm80"
Linkedin = "dustinmm80"
date = "2017-07-06T16:15:20-05:00"

+++

Understanding the needs of software users, empathizing with them and enhancing their user experience is Dustin's focus.

Recently, Dustin has been focused on: automation, 'the cloud', APIs, CI/CD pipelines and open-source software.
He loves being part of an organization that prioritizes a learning culture.
The agile process is a good fit for him; he likes working iteratively and getting ideas out the door in weeks instead of months.

Through writing and participating in conferences, Dustin tries to share the truths we uncover in our work with the world.