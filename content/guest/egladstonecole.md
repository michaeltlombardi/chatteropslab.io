+++
title = "Emily Gladstone Cole"
Website = ""
Twitter = "unixgeekem"
YouTube = ""
Thumbnail = "https://pbs.twimg.com/profile_images/596414478155026432/DmOXfl8e_400x400.jpg"
Instagram = ""
Pinterest = ""
Type = "guest"
Facebook = ""
GitHub = ""
Linkedin = ""
date = "2017-07-06T16:15:20-05:00"

+++

Emily is currently a Senior Incident Response Engineer for Machine Zone, Inc.
Emily has spent time performing critical organizational roles of security research, incident response, product security, system administrator, tech support, security expert, operations specialist, and project lead.
Emily specializes in Unix security and is a co-author of a book on Solaris Security for the SANS Institute.
She currently holds GSEC, GCED, GPPA, GCIH, and ITIL certifications, and is a Certified Scrum Master.