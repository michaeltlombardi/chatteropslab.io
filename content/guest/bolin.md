+++
title = "Brandon Olin"
Website = "https://devblackops.io"
Twitter = "devblackops"
YouTube = "channel/UCPAA3UOoho0ZDsXI8BWYrYw"
Thumbnail = "https://devblackops.io/images/profile.png"
Instagram = ""
Pinterest = ""
Type = "guest"
Facebook = ""
GitHub = "devblackops"
Linkedin = "brandonolin"
date = "2017-07-06T16:15:20-05:00"

+++

Brandon is a Lead Systems Engineer at Columbia Sportswear focusing on automation, configuration management, virtualization, and monitoring.
He spends much of his time exploring new technologies to drive the business forward and loves to apply ideas pioneered in the DevOps community to the traditional enterprise environment with the goal of solving real-world business problems.
Brandon is active in the PowerShell community with a number of projects published to the PowerShell Gallery.