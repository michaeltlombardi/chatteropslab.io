+++
date = "2017-03-10T13:00:30-06:00"
title = "About ChatterOps"
description = "Information about ChatterOps"

+++

## Why ChatterOps?

We noticed a gap in the coverage of projects and concepts - it is great to have an interview or demo of something, but almost nothing beats having access to smart people to talk about things.  That's where this community show comes in - we connect smart humans to each other and facillitate those discussions.

## When?

We record at least once per month via Google Hangouts and invite audience members to participate in the discussion live.