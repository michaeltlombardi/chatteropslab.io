+++
Description = "This episode we interview Samantha Ventura to learn about: bridging the gap between leaders and followers, soft skills, transition vs change, and positive ways to influence change. Then we’ll chat with the community at large about these topics and more"
PublishDate = "2017-04-26"
aliases = ["/bridging"]
author = "Mike"
date = "2017-04-26"
episode = "3"
episode_image = "img/logo.jpg"
explicit = "yes"
guests = ["sventura"]
images = ["img/episode/default-social.jpg"]
podcast_bytes = ""
podcast_duration = "1:13:45"
podcast_file = ""
sponsors = []
title = "Samantha Ventura + Bridging Gaps"
youtube = "A81i_4tvne4"

+++