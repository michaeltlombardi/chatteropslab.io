+++
Description = "This episode we interview Brandon Olin to see what he's been up to, talk about his project for a Slack chatbot written in PowerShell, and then chat with the community at large."
PublishDate = "2017-03-10"
aliases = ["/poshbot"]
author = "Mike"
date = "2017-03-10"
episode = "1"
episode_image = "img/logo.jpg"
explicit = "yes"
guests = ["bolin"]
images = ["img/episode/default-social.jpg"]
podcast_bytes = ""
podcast_duration = "1:03:36"
podcast_file = ""
sponsors = []
title = "Brandon Olin + PoshBot"
youtube = "gfSqNLBjg8Y"

+++

