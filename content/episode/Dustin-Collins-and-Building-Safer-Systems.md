+++
Description = "This episode we interview Dustin Collins to learn about building and executing safer workflows, accounting for the inescapability of mistakes in systems when humans get involved, and then chat with the community at large."
PublishDate = "2017-06-21"
aliases = ["/safersystems"]
author = "Mike"
date = "2017-06-21"
episode = "4"
episode_image = "img/logo.jpg"
explicit = "yes"
guests = ["dcollins"]
images = ["img/episode/default-social.jpg"]
podcast_bytes = ""
podcast_duration = "1:00:59"
podcast_file = ""
sponsors = []
title = "Dustin Collins + Building Safer Systems"
youtube = "lHyTxYUJlZ8"

+++

+ [The Summon Project](https://github.com/conjurinc/summon)
+ [DevOpsDays](https://www.devopsdays.org/)
+ [Sidney Dekker](http://sidneydekker.com/)
+ [Antifragile: Things That Gain from Disorder](https://www.amazon.com/Antifragile-Things-That-Disorder-Incerto/dp/0812979680)
+ [Dustin's DevOpsDays Ohio Talk](https://www.youtube.com/watch?v=DqaV9XbcPWU)