+++
Title = "Michael T Lombardi"
Twitter = "barbariankb"
Website = "https://michaeltlombardi.gitlab.io/"
Type = "host"
Linkedin = "michaeltlombardi"
GitHub = "michaeltlombardi"
Thumbnail = "https://avatars2.githubusercontent.com/u/14190564?v=3&u=e3c53cb6f3611b4742ae86261c2fa8f581489bf2&s=400"
Aka = ["jsmith", "jsmith2"]
+++
Mike is a human who makes mistakes, listens, and tries to improve. Passionate about making IT more humane. Software engineer at [Puppet](https://puppet.com)