# Contacting ChatterOps

You can [**email us**][email], hit us up on [twitter], or drop in and see us work in [**GitLab**][gitlab].

We would _love_ to have your feedback, especially if:

+ You have an idea for a new episode or a guest
+ You noticed innacuracies or issues with an episode
+ You'd like to come on board as a guest

We're generally open to all of your feedback - we want to make sure we're improving the show for you!

We _try_ to do most of our work in the open on [**GitLab**][gitlab], filing issues and updating things as we go.

[gitlab]:  https://gitlab.com/chatterops/
[email]:   mailto:chatteropsorg@gmail.com
[twitter]: https://twitter.com/chatteropsorg